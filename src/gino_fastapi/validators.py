"""Various validator models from pydantic."""

from pydantic import BaseModel  # pylint: disable=no-name-in-module


class Status(BaseModel):
    """Base status model."""

    message: str


class Token(BaseModel):
    """Token type."""

    access_token: str
    token_type: str


class TokenData(BaseModel):
    """Token data type."""

    username: str = None


class NewUser(BaseModel):
    """New User data type."""

    username: str
    password: str
    password_repeat: str
    first_name: str
    last_name: str


class PatchUser(BaseModel):
    username: str
    first_name: str
    last_name: str
    is_active: bool
    is_superuser: bool
