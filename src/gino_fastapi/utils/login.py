"""Login utils."""

import logging
from datetime import datetime, timedelta
from typing import Dict, Optional, Union

import jwt
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

from jwt import PyJWTError
from passlib.context import CryptContext

from gino_fastapi import config
from gino_fastapi.models.users import User
from gino_fastapi.validators import TokenData

LOGGER = logging.getLogger(__name__)


PASSWORD_CONTEXT = CryptContext(schemes=["bcrypt"], deprecated="auto")
OAUTH2_SCHEME = OAuth2PasswordBearer(tokenUrl="/token")


def verify_password(plain_password: str, hashed_password: str) -> bool:
    """Verify that the plain password matches the hashed password."""
    verified = bool(PASSWORD_CONTEXT.verify(plain_password, hashed_password))
    if verified:
        LOGGER.info("Password matched.")
    else:
        LOGGER.info("Password did not match")
    return verified


def get_password_hash(password: str) -> str:
    """Get a password hash."""
    LOGGER.info("Getting password hash.")
    hashed_password: str = PASSWORD_CONTEXT.hash(password)
    return hashed_password


async def authenticate_user(username: str, password: str) -> Optional[User]:
    """Authenticate the user."""
    LOGGER.info("Attempting to authenticate user %s", username)
    user = await User.query.where(User.username == username).gino.first()
    if not user:
        LOGGER.info("User with username %s not found.", username)
        authenticated_user = None
    elif not user.verify_password(password):
        LOGGER.info("Username found but password incorrect.")
        authenticated_user = None
    else:
        LOGGER.info("Found user for username %s", username)
        authenticated_user = user
    return authenticated_user


def create_access_token(*, data: dict, expires_delta: timedelta = None) -> bytes:
    """Create an access token for the user."""
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, config.SECRET_KEY, algorithm=config.ALGORITHM)
    LOGGER.info("Token created.")
    return encoded_jwt


async def get_current_user(token: str = Depends(OAUTH2_SCHEME)) -> Optional[User]:
    """Get the current user information using their jwt."""
    LOGGER.info("Getting current user.")
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, config.SECRET_KEY, algorithms=[config.ALGORITHM])
        username: Optional[str] = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
        LOGGER.info("Valid token for %s: %s", username, token_data)
    except PyJWTError:
        raise credentials_exception
    user: Optional[User] = await User.query.where(
        User.username == username
    ).gino.first()
    if user is None:
        raise credentials_exception
    LOGGER.info("Valid user found with token.")
    return user


async def get_current_active_user(
    current_user: User = Depends(get_current_user),
) -> User:
    """Get the current active user."""
    LOGGER.info("Checking if user is active.")
    if not current_user.is_active:
        LOGGER.info("User is not active.")
        raise HTTPException(status_code=400, detail="Inactive user")
    LOGGER.info("User is active.")
    return current_user


async def get_active_superuser(
    active_user: User = Depends(get_current_active_user),
) -> User:
    """Get the current active user."""
    LOGGER.info("Checking if user is active.")
    if not active_user.is_superuser:
        LOGGER.info("User is not a superuser.")
        raise HTTPException(status_code=403, detail="Unauthorized")
    LOGGER.info("User is a superuser.")
    return active_user


async def get_access_token(
    form_data: OAuth2PasswordRequestForm,
) -> Dict[str, Union[str, bytes]]:
    """Log a user in for their access token."""
    LOGGER.info("Got form data: %s", form_data)
    user = await authenticate_user(form_data.username, form_data.password)
    LOGGER.info("User authenticated.")
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=config.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}
