"""Basic autocrud utils."""

import logging
import types

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel, create_model  # pylint: disable=no-name-in-module
from typing import List
from gino_fastapi.models.users import User
from gino_fastapi.models import db
from gino_fastapi.utils.login import get_current_active_user, get_active_superuser

from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


LOGGER = logging.getLogger(__name__)


def add_crud_router(app, model):

    LOGGER.info("Loading model %s.", model)

    router = APIRouter()

    validator = model.get_validator()
    post_validator = (
        model.get_validator(method="post") if hasattr(model, "__post__") else validator
    )
    patch_validator = (
        model.get_validator(method="patch")
        if hasattr(model, "__patch__")
        else validator
    )

    @router.get("/{uid}", response_model=validator)
    async def get_instance(
        uid: int, current_user: User = Depends(get_current_active_user)
    ):
        LOGGER.info("Retrieving %s for uid %s", model.__name__, uid)
        instance = await model.get_or_404(uid)
        LOGGER.info("Retrieved %s for uid %s", model.__name__, uid)
        cleaned = validator(**instance.to_dict())
        return cleaned

    @router.get("/{uid}/all", response_model=List[validator])
    async def get_all(current_user: User = Depends(get_current_active_user)):
        LOGGER.info("Retrieving all instances for %s", model.__name__)
        raw_results = await db.all(model.query)
        results = [validator(**instance.to_dict()) for instance in raw_results]
        return results

    @router.post("/")
    async def add_instance(
        instance: post_validator, current_user: User = Depends(get_active_superuser)
    ):
        LOGGER.info("Creating %s for %s", model.__name__, instance)
        created = await model.create(**instance.dict())
        cur_validator = post_validator
        cleaned = cur_validator(**created.to_dict())
        LOGGER.info("Created %s: id=%s", model.__name__, created.id)
        return cleaned

    @router.delete("/{uid}")
    async def delete_instance(
        uid: int, current_user: User = Depends(get_current_active_user)
    ):
        LOGGER.info("Deleting %s for id %s", model.__name__, uid)
        instance = await model.get_or_404(uid)
        await instance.delete()
        LOGGER.info("Deleted %s for id %s", model.__name__, uid)
        return True

    @router.patch("/{uid}", response_model=validator)
    async def update_instance(
        uid: int,
        patch_data: patch_validator,
        current_user: User = Depends(get_current_active_user),
    ):
        instance = await model.get_or_404(uid)
        await instance.update(**patch_data.dict()).apply()
        cleaned = patch_validator(**instance.to_dict())
        return cleaned

    app.include_router(
        router,
        prefix=f"/{model._prefix()}",
        tags=model._tags(),
        responses={404: {"description": "Not found"}},
    )


def add_model_crud(app, models):
    for model in models:
        add_crud_router(app, model)
        LOGGER.info("Added CRUD for %s.", model.__name__)
    LOGGER.info("Finished adding CRUD for %s models.", len(models))
