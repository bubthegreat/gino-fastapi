import logging
import types

from gino.ext.starlette import Gino  # pylint: disable=no-name-in-module,import-error
from pydantic import BaseModel, create_model  # pylint: disable=no-name-in-module


from gino_fastapi import config

LOGGER = logging.getLogger(__name__)


class BaseValidator(BaseModel):
    id: int


db = Gino(
    dsn=config.DB_DSN,
    pool_min_size=config.DB_POOL_MIN_SIZE,
    pool_max_size=config.DB_POOL_MAX_SIZE,
    echo=config.DB_ECHO,
    ssl=config.DB_SSL,
    use_connection_for_request=config.DB_USE_CONNECTION_FOR_REQUEST,
    retry_limit=config.DB_RETRY_LIMIT,
    retry_interval=config.DB_RETRY_INTERVAL,
)
LOGGER.info("Database instance created: %s", db)


class CRUDBase(object):
    @classmethod
    def _get_table(cls):
        if not hasattr(cls, "__tablename__"):
            raise ValueError("__tablename__ must be defined on model %s", cls.__name__)
        return getattr(cls, "__tablename__")

    @classmethod
    def _prefix(cls):
        return cls._get_table()

    @classmethod
    def _tags(cls):
        return [cls._get_table().title()]

    @classmethod
    def _get_override_keys(cls, method, valid_keys, override_dict):
        """Get override keys when they're set for a method."""
        method_dunder = f"__{method}__"
        if not hasattr(cls, method_dunder):
            LOGGER.info(
                "No overrides found for method: '%s' - returning valid_keys", method
            )
            return valid_keys

        LOGGER.info("%s method validator.", method)
        overrides = getattr(cls, method_dunder)
        additional_excluded = overrides.get("exclude", [])
        LOGGER.info("Additional excluded: %s", additional_excluded)
        valid_keys = [key for key in valid_keys if key not in additional_excluded]
        override = overrides.get("override", {})
        additional_added = overrides.get("additional", [])
        for additional_key in additional_added:
            valid_keys.append(additional_key)
        for key, val in override.items():
            override_dict[key] = val
            valid_keys.append(key)
        LOGGER.info("override_dict: %s", override_dict)
        return valid_keys

    @classmethod
    def get_validator(cls, method=None):

        method_dunder = f"__{method}__" if method else None
        override_dict = {}
        additional_added = {}

        LOGGER.info("Getting initial keys for %s", cls.__name__)
        keys = [str(key) for key in cls.__dict__.keys()]
        if method_dunder:
            overrides = getattr(cls, method_dunder)
            additional_added = overrides.get("additional", {})
            if additional_added:
                LOGGER.info("Found additional keys to add: %s", additional_added)
            for additional_key in additional_added:
                keys.append(additional_key)
                LOGGER.info("Added additional key: %s", additional_key)
        valid_keys = [key for key in keys if not key.startswith("_")]
        LOGGER.info("Valid keys without exclusions: %s", valid_keys)

        if hasattr(cls, "__excluded__"):
            LOGGER.info("Found excluded")
            excl_keys = getattr(cls, "__excluded__")
            LOGGER.info("Excluded keys: %s", excl_keys)
            valid_keys = [key for key in valid_keys if key not in excl_keys]
            LOGGER.info("Valid keys with exclusions: %s", valid_keys)

        valid_keys = cls._get_override_keys(method, valid_keys, override_dict)
        LOGGER.info("Valid keys without overrides: %s", valid_keys)

        field_definitions = {}
        for key in valid_keys:
            col = getattr(cls, key, None)
            if key in override_dict:
                key = override_dict.get(key)
            if key in additional_added:
                field_definitions[key] = (additional_added[key], ...)
            if col is None:
                continue
            if isinstance(col, (types.MethodType, types.FunctionType)):
                # Don't snag on properties or methods.
                continue
            LOGGER.debug("Getting validator attribute for %s", key)
            field_definitions[key] = (col.type.python_type, ...)
        validator_name = f"{cls.__name__}{'.' + method if method else ''}"
        LOGGER.info(
            "Creating validator %s with keys: %s",
            validator_name,
            list(field_definitions.keys()),
        )
        validator = create_model(validator_name, **field_definitions)

        return validator
