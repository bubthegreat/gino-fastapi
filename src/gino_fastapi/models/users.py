"""Base model for users."""

from passlib.context import CryptContext
from gino_fastapi.models import db, CRUDBase


class User(db.Model, CRUDBase):
    __tablename__ = "users"
    __excluded__ = ["hashed_password", "is_active", "is_superuser"]
    __post__ = {
        "exclude": ["id"],
        "override": {"hashed_password": "password"},
        "additional": {"password_repeat": str},
    }
    __patch__ = {"exclude": ["username"]}

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.Unicode(), nullable=False)
    first_name = db.Column(db.Unicode(), nullable=False)
    last_name = db.Column(db.Unicode(), nullable=False)
    hashed_password = db.Column(db.Unicode(), nullable=False)
    is_active = db.Column(db.Boolean, default=False)
    is_superuser = db.Column(db.Boolean, default=False)

    def __str__(self) -> str:
        return self.full_name()

    def full_name(self) -> str:
        """
        Returns the full name if it exists, otherwise email.
        """
        if self.first_name or self.last_name:
            full_name = f"{self.first_name or ''} {self.last_name or ''}".strip()
        else:
            full_name = self.username
        return full_name

    def verify_password(self, password: str) -> bool:
        """Verify a password."""
        pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        pass_matches = bool(pwd_context.verify(password, self.hashed_password))
        return pass_matches

    @classmethod
    def hash_password(cls, password: str) -> str:
        pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
        hashed = pwd_context.hash(password)
        return hashed
