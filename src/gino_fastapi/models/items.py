from gino_fastapi.models import db, CRUDBase


class Item(db.Model, CRUDBase):
    __tablename__ = "items"
    __excluded__ = ["id"]

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(), nullable=False)
    description = db.Column(db.Unicode(), nullable=False)
