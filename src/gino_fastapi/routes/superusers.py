"""Basic superuser utils."""

import logging

from fastapi import APIRouter, Depends, HTTPException
from typing import List
from gino_fastapi.models.users import User
from gino_fastapi.models import db
from gino_fastapi.validators import PatchUser, NewUser
from gino_fastapi.utils.login import get_active_superuser
from gino_fastapi.routes.users import create_user


from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


LOGGER = logging.getLogger(__name__)


def add_superusers_router(app):

    LOGGER.info("Loading superuser routes.")

    router = APIRouter()

    validator = User.get_validator()
    post_validator = User.get_validator(method="post")
    patch_validator = User.get_validator(method="patch")

    @router.patch("/usermod", response_model=PatchUser)
    async def modify_existing_user(
        uid: int,
        patch_data: PatchUser,
        current_user: User = Depends(get_active_superuser),
    ):
        instance = await User.get_or_404(uid)
        existing_user = await User.query.where(
            User.username == patch_data.username
        ).gino.first()
        LOGGER.info("Existing user: %s", existing_user)
        if existing_user and patch_data.username != instance.username:
            LOGGER.info(
                "Cannot change username from %s to %s - the username is already in use.",
                patch_data.username,
                existing_user.username,
            )
            raise HTTPException(status_code=403, detail="Username already exists.")
        await instance.update(**patch_data.dict()).apply()
        cleaned = PatchUser(**instance.to_dict())
        return cleaned

    @router.post("/usermod",)
    async def add_initial_superuser(instance: NewUser):
        existing_users = len(await db.all(User.query))
        if existing_users:
            raise HTTPException(status_code=403, detail="Unauthorized")
        created = await create_user(instance)
        cur_validator = User.get_validator()
        cleaned = cur_validator(**created.to_dict())
        LOGGER.info("Created SuperUser: id=%s", created.id)
        return cleaned

    app.include_router(
        router,
        prefix=f"/su",
        tags=["Superuser"],
        responses={404: {"description": "Not found"}},
    )

    return app
