"""Basic superuser utils."""

import logging
import types

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel, create_model  # pylint: disable=no-name-in-module
from typing import List
from gino_fastapi.models.users import User
from gino_fastapi.models import db
from gino_fastapi.validators import PatchUser, NewUser
from gino_fastapi.utils.login import get_access_token


from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


LOGGER = logging.getLogger(__name__)


def add_token_router(app):

    LOGGER.info("Loading token routes.")

    router = APIRouter()

    @router.post("/")
    async def get_token(form_data: OAuth2PasswordRequestForm = Depends()):
        LOGGER.info("Attempting to get token data: %s", form_data)
        token = await get_access_token(form_data)
        LOGGER.info("Got token: %s", token)
        return token

    app.include_router(
        router,
        prefix=f"/token",
        tags=["Token"],
        responses={404: {"description": "Not found"}},
    )

    return app
