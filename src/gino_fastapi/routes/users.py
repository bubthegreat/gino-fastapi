"""Basic user utils."""

import logging
import types

from fastapi import APIRouter, Depends, HTTPException
from pydantic import BaseModel, create_model  # pylint: disable=no-name-in-module
from typing import List
from gino_fastapi.models.users import User
from gino_fastapi.validators import PatchUser, NewUser
from gino_fastapi.models import db
from gino_fastapi.utils.login import get_current_active_user, get_active_superuser

from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


LOGGER = logging.getLogger(__name__)

# TODO: Split out the general CRUD from the User CRUD and add a superuser CRUD endpoint for managing
# other users.


def add_users_router(app):

    LOGGER.info("Loading user routes.")

    router = APIRouter()

    validator = User.get_validator()
    post_validator = User.get_validator(method="post")
    patch_validator = User.get_validator(method="patch")

    @router.get("/{uid}", response_model=validator)
    async def get_instance(
        uid: int, current_user: User = Depends(get_current_active_user)
    ):
        if current_user.id != uid:
            # User creation is special, because it includes permissions
            # and access tokens, superuser, etc.
            if not current_user.is_superuser and current_user.is_active:
                LOGGER.info(
                    "Cannot get user information about others unless you're a superuser."
                )
                raise HTTPException(status_code=403, detail="Unauthorized")
        LOGGER.info("Retrieving User for uid %s", uid)
        instance = await User.get_or_404(uid)
        LOGGER.info("Retrieved User for uid %s", uid)
        cleaned = validator(**instance.to_dict())
        return cleaned

    @router.get("/{uid}/all", response_model=List[validator])
    async def get_all(current_user: User = Depends(get_current_active_user)):
        if not current_user.is_superuser and current_user.is_active:
            LOGGER.info("Cannot list all users unless you're a super user and active.")
            raise HTTPException(status_code=403, detail="Unauthorized")
        LOGGER.info("Retrieving all instances for User")
        raw_results = await db.all(User.query)
        results = [validator(**instance.to_dict()) for instance in raw_results]
        return results

    @router.post("/")
    async def add_instance(
        instance: post_validator, current_user: User = Depends(get_active_superuser)
    ):
        LOGGER.info("Creating User for %s", instance)
        if not current_user.is_superuser and current_user.is_active:
            LOGGER.info(
                "Cannot create another user unless you're a super user and active."
            )
            raise HTTPException(status_code=403, detail="Unauthorized")
        created = await create_user(instance)
        cur_validator = User.get_validator()
        cleaned = cur_validator(**created.to_dict())
        LOGGER.info("Created User: id=%s", created.id)
        return cleaned

    @router.delete("/{uid}")
    async def delete_instance(
        uid: int, current_user: User = Depends(get_current_active_user)
    ):

        if current_user.id != uid:
            if not current_user.is_superuser and current_user.is_active:
                LOGGER.info(
                    "Cannot delete another user unless you're a super user and active."
                )
                raise HTTPException(status_code=403, detail="Unauthorized")

        LOGGER.info("Deleting User for id %s", uid)
        instance = await User.get_or_404(uid)
        await instance.delete()
        LOGGER.info("Deleted User for id %s", uid)
        return True

    @router.patch("/{uid}", response_model=validator)
    async def update_instance(
        uid: int,
        patch_data: patch_validator,
        current_user: User = Depends(get_current_active_user),
    ):

        if current_user.id != uid:
            if not current_user.is_superuser and current_user.is_active:
                LOGGER.info(
                    "Cannot modify another user unless you're a super user and active."
                )
                raise HTTPException(status_code=403, detail="Unauthorized")
        instance = await User.get_or_404(uid)
        await instance.update(**patch_data.dict()).apply()
        cleaned = patch_validator(**instance.to_dict())
        return cleaned

    app.include_router(
        router,
        prefix=f"/user",
        tags=["User"],
        responses={404: {"description": "Not found"}},
    )

    @app.get("/user/current/", tags=["User"])
    async def read_current_user(current_user: User = Depends(get_current_active_user)):
        LOGGER.info("Ahead")
        cleaned = PatchUser(**current_user.to_dict())
        LOGGER.info("After")
        return cleaned

    return app


async def create_user(new_user: User.get_validator()):
    nudict = new_user.dict()
    password = nudict.pop("password")
    password_repeat = nudict.pop("password_repeat")
    if password != password_repeat:
        raise HTTPException(status_code=403, detail="Mismatched passwords")
    hashed_password = User.hash_password(password)
    nudict["hashed_password"] = hashed_password
    existing_users = len(await db.all(User.query))
    LOGGER.info("Number of existing users: %s", existing_users)
    if not existing_users:
        LOGGER.info("No existing users, creating superuser")
        nudict["is_superuser"] = True
        LOGGER.info("No existing users, marking as active")
        nudict["is_active"] = True

    user = await User.create(**nudict)

    return user
