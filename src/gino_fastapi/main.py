import logging

from fastapi import FastAPI, APIRouter, Depends, HTTPException
from fastapi.security import OAuth2PasswordRequestForm

import uvicorn

from gino_fastapi.models import db
from gino_fastapi.models.items import Item
from gino_fastapi.models.users import User
from gino_fastapi.utils.crud import add_model_crud
from gino_fastapi.routes.users import add_users_router
from gino_fastapi.routes.superusers import add_superusers_router
from gino_fastapi.routes.token import add_token_router
from gino_fastapi.utils.login import (
    get_current_active_user,
    get_access_token,
    get_active_superuser,
)
from gino_fastapi.validators import PatchUser, NewUser

LOGGER = logging.getLogger(__name__)

# Add autocrud models here
CRUD_MODELS = [
    Item,
]

# Add special routers here.
ROUTERS = [
    add_token_router,
    add_users_router,
    add_superusers_router,
]


def get_app():
    app = FastAPI(title="This is a basic GINO FastAPI implementation!")
    LOGGER.info("Initializing db.")
    db.init_app(app)
    LOGGER.info("Initialization of db complete.")

    # Main CRUD routers added.
    LOGGER.info("Adding CRUD routers for %s models", len(CRUD_MODELS))
    add_model_crud(app, CRUD_MODELS)

    LOGGER.info("Adding special routers.")
    for add_router_func in ROUTERS:
        add_router_func(app)
        LOGGER.info("Added special router %s", add_router_func.__name__)

    return app


def run():
    """Run through uvicorn when run."""
    uvicorn.run(
        "gino_fastapi.asgi:app",
        host="localhost",
        port=8000,
        reload=True,
        log_level="trace",
    )
