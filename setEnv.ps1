Write-Output "setEnv running..."

$Env:DB_DRIVER = "postgresql"
Write-Output "DB_DRIVER set to $Env:DB_DRIVER"
$Env:DB_HOST = "localhost"
Write-Output "DB_HOST set to $Env:DB_HOST"
$Env:DB_PORT = "5432"
Write-Output "DB_PORT set to $Env:DB_PORT"
$Env:DB_USER = "postgresql"
Write-Output "DB_USER set to $Env:DB_USER"
$Env:DB_PASSWORD = "postgresql"
Write-Output "DB_PASSWORD set to $Env:DB_PASSWORD"
$Env:DB_RETRY_LIMIT = 10
Write-Output "DB_RETRY_LIMIT set to $Env:DB_RETRY_LIMIT"
$Env:DB_RETRY_INTERVAL = 5
Write-Output "DB_RETRY_INTERVAL set to $Env:DB_RETRY_INTERVAL"
$Env:DB_DATABASE = "gino_fastapi_db"
Write-Output "DB_DATABASE set to $Env:DB_DATABASE"
$Env:PYTHONDONTWRITEBYTECODE = 1

# Edit this if you want to enable the root logger.
# $Env:gino_fastapi_LOG_ALL_THE_THINGS = ""
# Write-Output "gino_fastapi_LOG_ALL_THE_THINGS set to true"
