# Installation

1. Install poetry

### Windows
```powershell
Invoke-WebRequest https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | -OutFile get-poetry.py
python get-poetry.py
```

### Linux
```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
```

2. Install our package

```
poetry install
```

```
poetry run uvicorn gino_fastapi.asgi:app --reload
```