import pytest
from fastapi import FastAPI
from gino_fastapi.utils.crud import add_model_crud
from gino_fastapi.main import CRUD_MODELS

ALL_CRUD_ROUTES = set([
    'get_instance',
    'get_all',
    'add_instance',
    'delete_instance',
    'update_instance',
])

@pytest.mark.parametrize('crud_model', CRUD_MODELS)
def test_crud_routes_added(crud_model, app_routes):
    model_name = crud_model.__tablename__.title()
    crud_routes = app_routes.get(model_name)
    assert crud_routes, f'No crud routes found for {crud_model}'
    assert set(crud_routes) == ALL_CRUD_ROUTES, f'Missing expected crud routes for model {crud_model}'
