import pytest
from fastapi import FastAPI
from gino_fastapi.routes.users import add_users_router

def test_user_routes_added():
    app = FastAPI()
    add_users_router(app)

    actual_routes = sorted([route.name for route in app.routes])
    expected_routes = [
        'add_instance',
        'delete_instance',
        'get_all',
        'get_instance',
        'openapi',
        'read_current_user',
        'redoc_html',
        'swagger_ui_html',
        'swagger_ui_redirect',
        'update_instance',
    ]

    assert actual_routes == expected_routes
