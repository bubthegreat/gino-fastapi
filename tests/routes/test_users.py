from gino_fastapi.routes.users import create_user
from gino_fastapi.models.users import User
from fastapi import HTTPException

import pytest


@pytest.mark.asyncio
async def test_create_user_good(pg_db):
    """Test the create_user function with a good config."""
    # create
    new_user_dict = {
        "username": 'fake_user',
        "password": 'asdf1234',
        "password_repeat": 'asdf1234',
        "first_name": 'first_name',
        "last_name": 'last_name',
    }
    validator = User.get_validator(method='post')
    validated_user_obj = validator(**new_user_dict)
    user = await create_user(validated_user_obj)

    assert user is not None
    assert user.username == 'fake_user', "username isn't setting properly"
    assert user.verify_password('asdf1234'), "hashed password isn't setting properly"
    assert user.first_name == 'first_name', "first name isn't setting properly"
    assert user.last_name == 'last_name', "last name isn't setting properly"

@pytest.mark.asyncio
@pytest.mark.parametrize('pass1,pass2', [
    ('asdf1234', 'jkl;7890'),
    ('asdf1234', 'ASDF1234'),
])
async def test_create_user_bad_password_repeat(pass1, pass2, pg_db):
    """Test the create_user function with a mismatched password fails."""
    # create
    new_user_dict = {
        "username": 'fake_user',
        "password": pass1,
        "password_repeat": pass2,
        "first_name": 'first_name',
        "last_name": 'last_name',
    }
    validator = User.get_validator(method='post')
    validated_user_obj = validator(**new_user_dict)
    with pytest.raises(HTTPException) as excinfo:
        await create_user(validated_user_obj)
    assert excinfo.value.status_code == 403
    assert excinfo.value.detail == "Mismatched passwords"
