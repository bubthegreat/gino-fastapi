import pytest
from fastapi import FastAPI
from gino_fastapi.routes.token import add_token_router

def test_user_routes_added():
    app = FastAPI()
    add_token_router(app)

    actual_routes = sorted([route.name for route in app.routes])
    expected_routes = [
        'get_token',
        'openapi',
        'redoc_html',
        'swagger_ui_html',
        'swagger_ui_redirect'
    ]

    assert actual_routes == expected_routes
