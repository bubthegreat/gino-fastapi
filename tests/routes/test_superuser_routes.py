import pytest
from fastapi import FastAPI
from gino_fastapi.routes.superusers import add_superusers_router

def test_superuser_routes_added():
    app = FastAPI()
    add_superusers_router(app)

    actual_routes = sorted([route.name for route in app.routes])
    expected_routes = [
        'add_initial_superuser',
        'modify_existing_user',
        'openapi',
        'redoc_html',
        'swagger_ui_html',
        'swagger_ui_redirect'
    ]

    assert actual_routes == expected_routes
