import pytest
from alembic.config import main
from starlette.testclient import TestClient
from gino_fastapi import config
from gino_fastapi.models import db
from gino_fastapi.main import get_app


@pytest.fixture
async def pg_db():

    main(["--raiseerr", "upgrade", "head"])

    await db.set_bind(f'postgresql://{config.DB_USER}:{config.DB_PASSWORD}@{config.DB_HOST}:{config.DB_PORT}/{config.DB_DATABASE}')
    # await db.set_bind("sqlite:///./test.db")
    await db.gino.create_all()
    yield db

    main(["--raiseerr", "downgrade", "base"])

@pytest.fixture
def app_routes():
    app = get_app()
    all_routes = {}
    for route in app.routes:
        if hasattr(route, 'tags'):
            route_tag = route.tags[0]
            if not all_routes.get(route_tag):
                all_routes[route_tag] = []
            all_routes[route_tag].append(route.name)
    return all_routes