import pytest
from gino_fastapi.models.items import Item

from asyncpg.exceptions import NotNullViolationError

@pytest.mark.asyncio
@pytest.mark.parametrize('test_name', [
    'test_name',
])
@pytest.mark.parametrize('test_description', [
    'test_description'
])
async def test_item_creation(test_name, test_description, pg_db):
    # create
    item_data = {
        "name": test_name,
        "description": test_description,
    }

    item = await Item.create(**item_data)
    assert item is not None
    assert item.name == 'test_name', "name isn't setting properly"
    assert item.description == 'test_description', "description isn't setting properly"


@pytest.mark.asyncio
@pytest.mark.parametrize('null_key', [
    'name',
    'description',
])
async def test_null_item_key_fails(null_key, pg_db):
    # create
    item_data = {
        "name": "test_name",
        "description": "test_description",
    }
    item_data[null_key] = None

    with pytest.raises(NotNullViolationError):
        await Item.create(**item_data)

