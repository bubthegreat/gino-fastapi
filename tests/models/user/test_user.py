import pytest
from gino_fastapi.models.users import User

from asyncpg.exceptions import NotNullViolationError

@pytest.mark.asyncio
@pytest.mark.parametrize('is_active', [
    True, False, None
])
@pytest.mark.parametrize('is_superuser', [
    True, False, None
])
async def test_user_creation(is_active, is_superuser, pg_db):
    # create
    user_data = {
        "username": 'fake_user',
        "hashed_password": 'hashed_password',
        "first_name": 'first_name',
        "last_name": 'last_name',
        "is_active": is_active,
        "is_superuser": is_superuser,
    }

    user = await User.create(**user_data)
    assert user is not None
    assert user.username == 'fake_user', "username isn't setting properly"
    assert user.hashed_password == 'hashed_password', "hashed password isn't setting properly"
    assert user.first_name == 'first_name', "first name isn't setting properly"
    assert user.last_name == 'last_name', "last name isn't setting properly"
    assert user.is_active == is_active, "active permission isn't setting properly"
    assert user.is_superuser == is_superuser, "Superuser permission isn't setting properly"


@pytest.mark.asyncio
@pytest.mark.parametrize('is_active', [
    True, False, None
])
@pytest.mark.parametrize('is_superuser', [
    True, False, None
])
@pytest.mark.parametrize('null_key', [
    'username',
    'hashed_password',
    'first_name',
    'last_name',
])
async def test_no_username_fails(null_key, is_active, is_superuser, pg_db):
    # create
    user_data = {
        "username": "fake_user",
        "hashed_password": "hashed_password",
        "first_name": "first_name",
        "last_name": "last_name",
        "is_active": is_active,
        "is_superuser": is_superuser,
    }
    user_data[null_key] = None

    with pytest.raises(NotNullViolationError):
        await User.create(**user_data)

